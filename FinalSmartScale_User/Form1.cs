﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Windows.Forms;
using SimpleWifi;

namespace FinalSmartScale_User
{
    public partial class Form1 : Form
    {
        private static Wifi wifi;
        string dataRecieve;
        double dataCv1;
        double dataCv2;
        double m_val;
        double c_val;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var ports = SerialPort.GetPortNames();
            cb_comport.DataSource = ports;

            wifi = new Wifi();

            List<AccessPoint> asp = wifi.GetAccessPoints();

            foreach (AccessPoint ap in asp)
            {
                ListViewItem lvItem = new ListViewItem(ap.Name);
                cb_ssid.Items.Add(ap.Name);
            }
            cb_ssid.SelectedIndex = 0;
        }

        private void Btn_connectPort_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you confirm connect port?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (cb_comport.SelectedIndex > -1)
                {
                    MessageBox.Show(String.Format("You select port '{0}'", cb_comport.SelectedItem));
                    serialPort1.PortName = cb_comport.SelectedItem.ToString();                  
                    serialPort1.Open();
                    //serialPort1.Write(";serialNum;\r\n");
                    Console.WriteLine("Connect: " + serialPort1.PortName);
                }
                else
                {
                    MessageBox.Show("Please select a port first");
                }
            }
        }

        private void Btn_cal15_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you sure cal 1.5 kg. ?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //serialPort1.Open();
                    serialPort1.Write(";" + "cv1" + ";\r");
                    Console.WriteLine("cv1 complete");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_cal30_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you sure cal 3.0 kg. ?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //serialPort1.Open();
                    serialPort1.Write(";" + "cv2" + ";\r");
                    Console.WriteLine("cv2 complete");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_connect_Click(object sender, EventArgs e)
        {
            try
            {
                string ssidName = cb_ssid.SelectedItem.ToString();
                string passWord = tb_pass.Text;
                //serialPort1.Open();
                if (MessageBox.Show("Do you sure confirm. ?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //serialPort1.Open();
                    serialPort1.Write(";ssid;" + ssidName + ";pass;" + passWord + ";\r");

                    Console.WriteLine("ssid complete");
                    Console.WriteLine("pass complete");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cal_calibration()
        {
            double del_y = 1500;
            double del_x = dataCv2 - dataCv1;
            m_val = del_y / del_x;
            double multi = m_val * dataCv1;
            c_val = del_y - multi;

            serialPort1.Write(";" + "M_value" + ";" + m_val + ";" + "C_value" + ";" + c_val + "\r" + "\n");
            //Thread.Sleep(50);
            Console.WriteLine("M_value: " + m_val);
            Console.WriteLine("C_value: " + c_val);
            //serialPort1.Close();
        }

        private void SerialPort1_DataReceived_1(object sender, SerialDataReceivedEventArgs e)
        {
            dataRecieve = serialPort1.ReadExisting();
            this.Invoke(new EventHandler(ShowData));
        }

        private void ShowData(object sender, EventArgs e)
        {
            Console.WriteLine(dataRecieve);
            if (dataRecieve.Contains("cv1 is "))
            {
                int begin = dataRecieve.IndexOf('&');
                int end = dataRecieve.IndexOf('#');
                //dataCv1 = Convert.ToDouble(data.Substring(7, dataLength));
                dataCv1 = Convert.ToDouble(dataRecieve.Substring(begin + 1, end - begin - 1));
                Console.WriteLine(dataCv1);
                //serialPort1.Close();
            }
            else if (dataRecieve.Contains("cv2 is "))
            {
                int begin = dataRecieve.IndexOf('&');
                int end = dataRecieve.IndexOf('#');
                //dataCv1 = Convert.ToDouble(data.Substring(7, dataLength));
                dataCv2 = Convert.ToDouble(dataRecieve.Substring(begin + 1, end - begin - 1));
                Console.WriteLine(dataCv2);
                cal_calibration();

            }
            else if (dataRecieve.Contains("ScaleNo is "))
            {
                int begin = dataRecieve.IndexOf('&');
                int end = dataRecieve.IndexOf('#');
                //dataCv1 = Convert.ToDouble(data.Substring(7, dataLength));
                string str_serialNum = dataRecieve.Substring(begin + 1, end - begin - 1);
                lb_serialNum.Text = str_serialNum;
                Console.WriteLine("Serial Number: " + str_serialNum);
                //serialPort1.Close();
            }

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you update http?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    serialPort1.Write(";url;" + textBox1.Text + ";path;" + textBox2.Text + ";port;" + textBox3.Text + ";\r");
                    Console.WriteLine("update http");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
